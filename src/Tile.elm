
module Tile exposing (..)

import Html exposing (Html)
import Color exposing (..)
import TypedSvg exposing (..)
import TypedSvg.Attributes exposing (..)
import TypedSvg.Types exposing (..)

type Kind
  = Land
  | Water

-- TODO: Add aridness and rockiness.
-- TODO: Think out "unknown" and "fog of war" states.
type alias Tile =
  { kind   : Kind
  , height : Int
  }

view : Tile -> Html msg
view tile =
  g []
    [ polyline
      [ fill (Fill (colorOf tile))
      , hexContour
      ] []
    ]

hexContour = points
  [(0, 15), (15, 30), (45, 30), (60, 15), (45, 0), (15, 0), (0, 15)]

-- TODO: Use aridness and rockiness to draw tile.
colorOf : Tile -> Color
colorOf tile =
  let level = tile.height |> levelOfMax 5000
  in
    if tile.kind == Land
    then interpolate landColors  tile.height
    else interpolate waterColors tile.height

landColors : List (Int, Color)
landColors =
  [ (0,    rgb 1   102 94)
  , (1000, rgb 90  180 172)
  , (2000, rgb 199 234 229)
  , (3000, rgb 246 232 195)
  , (4000, rgb 216 179 101)
  , (4000, rgb 140 81  10)
  ]

waterColors : List (Int, Color)
waterColors =
  [ (0,    rgb 241 238 246)
  , (1000, rgb 208 209 230)
  , (2000, rgb 166 189 219)
  , (3000, rgb 116 169 207)
  , (4000, rgb 43  140 190)
  , (5000, rgb 4   90  141)
  ]

rgb : Int -> Int -> Int -> Color
rgb r g b = Color.fromRgba
  { red   = toFloat r / 256
  , green = toFloat g / 256
  , blue  = toFloat b / 256
  , alpha = 1
  }

interpolate : List (Int, Color) -> Int -> Color
interpolate list i = case list of
  [(x, col)] -> col
  (x, col1) :: (y, col2) :: rest ->
    if x <= i && y > i then
      mix col1 col2 (toFloat (i - x) / toFloat (y - x))
    else
      interpolate ((y, col2) :: rest) i

  _ -> black

mix : Color -> Color -> Float -> Color
mix col1 col2 x =
  let
    col1e = Color.toRgba col1
    col2e = Color.toRgba col2
    mixA a b = a * (1 - x) + b * x
    col   = Color.fromRgba
      { red   = mixA col1e.red   col2e.red
      , green = mixA col1e.green col2e.green
      , blue  = mixA col1e.blue  col2e.blue
      , alpha = 1
      }
  in col

levelOfMax : Int -> Int -> Float
levelOfMax max i = toFloat (max - i) / toFloat max

makeTile : Kind -> Tile
makeTile kind =
  { kind      = kind
  , height    = 5000
  }

main = svg [] <| [view { kind = Water, height = 2500 }]