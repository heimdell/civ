
module Terrain exposing (..)

import Color exposing (..)
import Browser exposing (..)
import Browser.Events as Event
import Dict exposing (..)
import Json.Decode
import Html exposing (Html)
import TypedSvg exposing (..)
import TypedSvg.Attributes exposing (..)
import TypedSvg.Types exposing (..)

import Tile exposing (Tile)

import Debug

type alias Terrain =
  { tiles : Dict Point Tile
  , x     : Range Int        -- map border
  , y     : Range Int        -- -"-
  , z     : Range Int        -- -"-
  , focus : Point            -- camera focus
  }

type alias Range a =
  { min : a
  , max : a
  }

type alias Point = (Int, Int)

-- TODO: Make map inifinite, by tiling the plane with the finite part.
--       This should allow for efficient pathfinding that knows the map is
--       actually finite.
getTile : Terrain -> Point -> Maybe Tile
getTile t (x, y) = Dict.get (x, y) t.tiles

allPoints : Terrain -> List Point
allPoints t =
  let
    xs = List.range t.x.min t.x.max
    ys = List.range t.y.min t.y.max

    hexagon =
      xs |> List.concatMap (\x ->
      ys |> List.concatMap (\y ->
        if x + y >= t.z.min && x + y <= t.z.max
        then [(x, y)]
        else []
      ))
  in
    hexagon

genPoints : (Point -> Tile) -> Terrain -> Terrain
genPoints gen t =
  let
    points = allPoints t
  in
    { t
    | tiles = Dict.fromList <| zip points <| List.map gen points
    }

-- TODO: Add more terrain generators, add ability to combine them
--       (for each point, select the one with max height, etc).
volcano : Int -> Int -> Terrain
volcano r r2 = ofRadius r |> genPoints (descent r2)

ofRadius : Int -> Terrain
ofRadius r =
  let
    span = { min = -r, max = r }
  in
    { tiles = Dict.empty
    , x     = span
    , y     = span
    , z     = span
    , focus = (0, 0)
    }

descent : Int -> Point -> Tile
descent r (x, y) =
  let
    endHeight = 5000 - (5000 * (distance (0, 0) (x, y)) // r)
  in
    { kind   = if endHeight < 0 then Tile.Water else Tile.Land
    , height = abs endHeight
    }

-- TODO: Move to Utils.
zip : List a -> List b -> List (a, b)
zip az bz = case (az, bz) of
  (a :: aRest, b :: bRest) -> (a, b) :: zip aRest bRest
  _                        -> []

-- TODO: Instead of drawing all tiles, draw only visible.
-- TODO: Find an algorithm to calculate all visible
-- TODO: Add ability to zoom in/out/return-to-normal.
-- TODO: Zooming in should improve drawing efficiency.
-- TODO: If rasterised images are used, make sure the versions
--       for each zoom level exist.
view : Terrain -> Html msg
view t =
  g [] <|
    ( t.tiles
    |> Dict.toList
    |> List.map
      (\(point, tile) ->
        -- TODO: Push the transform to the tile itself.
        g [transform <| dislocation <| scalar (+) t.focus point]
        [ Tile.view tile
        -- , text_ [] [Html.text (Debug.toString tile.height)]
        ]
      )
    )
    ++
      [ polyline
        [ fill FillNone
        , stroke red
        , strokeWidth (Px 2.5)
        , strokeLinejoin StrokeLinejoinBevel
        , Tile.hexContour
        , transform <| dislocation (0, 0)
        ] []
      ]

scalar : (Int -> Int -> Int) -> Point -> Point -> Point
scalar op (a, b) (c, d) = (op a c, op b d)

dislocation : Point -> List Transform
dislocation (x, y) =
  [ Translate
    ((toFloat (x + y) - 0.5) * 45)
    ((toFloat (x - y) - 3)   * 15)
  ]

-- TODO: Change message type from Point to more richer, populate with
--       reactions.
update : Point -> Terrain -> Terrain
update (dx, dy) t =
  let
    (x, y) = t.focus
  in
    {t | focus = (x + dx, y + dy)}

-- TODO: Use `application`, not `element`. Generate terrain properly at random.
-- TODO: Add a starting screen to make new game or load one.
-- TODO: Make a server/thread-local-storage device to store the game.
-- TODO: Make a server/thread-local-storage thing to save/load the game as JSON.
main : Program () Terrain Point
main = element
  { init          = \()    -> (volcano 20 7, Cmd.none)
  , update        = \msg s -> (update msg s, Cmd.none)
  , view          = \t     -> w1024x768 [view t]
  , subscriptions = \s     -> wasd
  }

-- TODO: Register more buttons. Throttle the keypresses.
wasd : Sub Point
wasd =
  Event.onKeyDown (Json.Decode.field "key" Json.Decode.string)
  |> Sub.map toPoint

w1024x768 : List (Html msg) -> Html msg
w1024x768 = svg [viewBox -512 -384 1024 768, width (Px 1024), height (Px 768)]

-- TODO: Make sue this works with the tiled metrics.
-- TODO: All calculations with points should NEVER refer to (x, y)
--       outside of Point module. The paths found should be List Dir.
--       We should be only able to read (x, y) for the purpose of debug.
distance : Point -> Point -> Int
distance origin point =
  let
    (dx, dy) = scalar (-) origin point
  in
    if dx == 0 || dy == 0
    || signum dx == signum dy
    then abs dx + abs dy
    else Basics.max (abs dx) (abs dy)

signum : number -> number
signum n =
  if n == 0 then  0 else
  if n <  0 then -1 else 1

-- TODO: Make it "-> Message".
toPoint : String -> Point
toPoint s = Debug.log "p" <| case s of
  "w" -> ( 1, -1)
  "s" -> (-1,  1)
  "q" -> ( 1,  0)
  "e" -> ( 0, -1)
  "a" -> ( 0,  1)
  "d" -> (-1,  0)
  _   -> ( 0,  0)
