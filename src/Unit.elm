
module Unit exposing (..)

import Html exposing (Html)
import Color exposing (..)
import TypedSvg exposing (..)
import TypedSvg.Attributes exposing (..)
import TypedSvg.Types exposing (..)

type Origin
  = Human
  | Native

type Chassis
  = Infantry
  | Foil
  | Copter
  | Hovercraft

type alias Unit =
  { origin  : Origin
  , chassis : Chassis
  , status  : Status
  , proto   : Status
  }

type alias Status =
  { hp     : Float
  , dmg    : Float
  , def    : Float
  , steps  : Int
  , morale : Int
  }

type alias MoveOpts opts =
  { opts
  | alongRoad  : Bool
  , alongRiver : Bool
  , toFungus   : Bool
  , fromFungus : Bool
  , friendDest : Bool
  , hasEmpaty  : Bool
  }

view : Unit -> Html msg
view unit = case unit.chassis of
  Infantry   -> polygon [fill (Fill green), points [(5,15), (10,5), (15,15)]] []
  Foil       -> polygon [fill (Fill green), points [(0,7),  (5,12), (15,12), (20,7)]] []
  Copter     -> circle  [fill (Fill green), cx (Px 10), cy (Px 10), r (Px 8)] []
  Hovercraft -> polygon [fill (Fill green), points [(15,15), (15,5), (5,5), (5,15)]] []

spendStepsMoving : MoveOpts opts -> Unit -> Unit
spendStepsMoving {alongRoad, alongRiver, toFungus, fromFungus, friendDest, hasEmpaty} unit =
  unit |> (modifyStatus << modifySteps) (\steps ->
    if flying unit then
      steps - 2
    else if (unit.origin == Native || hasEmpaty) && toFungus && fromFungus then
      steps - 1
    else if unit.origin == Human && not fromFungus && toFungus && not friendDest then
      steps - 4
    else if alongRoad || alongRiver then
      steps - 1
    else
      steps - 2
  )

flying : Unit -> Bool
flying unit = unit.chassis == Copter

seaborn : Unit -> Bool
seaborn unit = unit.chassis == Foil

hovering : Unit -> Bool
hovering unit = unit.chassis == Hovercraft

modifySteps : (Int -> Int) -> Status -> Status
modifySteps f status =
  {status | steps = f status.steps}

modifyStatus : (Status -> Status) -> Unit -> Unit
modifyStatus f unit =
  {unit | status = f unit.status}

make chassis =
  { origin  = Human
  , chassis = chassis
  , status  = { hp = 0, dmg = 0, def = 0, steps = 0, morale = 0}
  , proto   = { hp = 0, dmg = 0, def = 0, steps = 0, morale = 0}
  }

main = svg [] <| [g [] [view <| make Hovercraft]]
