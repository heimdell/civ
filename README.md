
# Installation

```sh
sudo npm i -g elm  # install elm compiler
elm make           # build the project
elm reactor        # open localhost:8000 in your browser, navigate to src/Terrain.elm
```

# What to do

Try pressing `W`, `S`, `A`, `D`, `Q` and `E`.